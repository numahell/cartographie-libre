---
title: "Outils libres pour la cartographie"
order: 0
in_menu: true
---
Voici quelques ressources et outils pour de la cartographie libre.

## La carte libre

[OpenStreetMap](https://www.openstreetmap.org), la cartographie libre.

## Créer des cartes en ligne

  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/uMap.png">
    </div>
    <div>
      <h3>uMap</h3>
      <p>Un outil en ligne pour personnaliser vos cartes.</p>
      <div>
        <a href="https://beta.framalibre.org/notices/umap.html">Vers la notice Framalibre</a>
        <a href="https://github.com/umap-project/umap/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/GoGoCarto.png">
    </div>
    <div>
      <h3>GoGoCarto</h3>
      <p>Créez des cartes géographiques personnalisées, seul·e ou de manière collaborative.</p>
      <div>
        <a href="https://beta.framalibre.org/notices/gogocarto.html">Vers la notice Framalibre</a>
        <a href="https://gogocarto.fr/projects">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/Leaflet.png">
    </div>
    <div>
      <h3>Leaflet</h3>
      <p>Librairie Javascript pour générer des cartes interactive et dessiner dessus, ajouter des marqueurs...</p>
      <div>
        <a href="https://beta.framalibre.org/notices/leaflet.html">Vers la notice Framalibre</a>
        <a href="http://leafletjs.com/">Vers le site</a>
      </div>
    </div>
  </article>

## Manipuler des données géographiques

  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/Qgis.jpg">
    </div>
    <div>
      <h3>Qgis</h3>
      <p>QGIS, ou le SIG libre pour tous !</p>
      <div>
        <a href="https://beta.framalibre.org/notices/qgis.html">Vers la notice Framalibre</a>
        <a href="https://qgis.org">Vers le site</a>
      </div>
    </div>
  </article>

## Applications mobile

  <article class="framalibre-notice">
    <div>
      <img src="">
    </div>
    <div>
      <h3>Organic Maps</h3>
      <p>Application libre de cartes hors ligne et navigation GPS basée sur les données OpenStreetMap - Pour Android et iOS</p>
      <div>
        <a href="https://beta.framalibre.org/notices/organic-maps.html">Vers la notice Framalibre</a>
        <a href="https://organicmaps.app/fr/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/SreetComplete.svg">
    </div>
    <div>
      <h3>StreetComplete</h3>
      <p>Contribuez à OpenStreetMap en répondant à des questions simples.</p>
      <div>
        <a href="https://beta.framalibre.org/notices/streetcomplete.html">Vers la notice Framalibre</a>
        <a href="https://streetcomplete.app/">Vers le site</a>
      </div>
    </div>
  </article> 